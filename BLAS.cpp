#include <BLAS.h>

/*
// 返回随机整数值
int MyRandomInt()
{
    int rnd = time(0);//随机数种子
    srand(rnd);
    int shift = rand();
    if (shift % 2 == 0)
    {
        shift *= -1;
    }
    return shift;
}

// 返回一个满足高斯分布的随机浮点数
double Gauss(double sys_err_ave,double sys_err_var)
{
    //设置高斯分布的均值和方差
    double mean = sys_err_ave;
    double r_ms = sqrt(sys_err_var);

    double u_1, u_2, V_1, V_2;
    double W = 0;
    double dst = 0;
    bool ok = false;
    while (!ok)
    {
        //RAND_MAX一般为32727
        u_1 = (double)rand() / RAND_MAX;
        u_2 = (double)rand() / RAND_MAX;
        V_1 = 2 * u_1 - 1;
        V_2 = 2 * u_2 - 1;
        W = V_1 * V_1 + V_2 * V_2;
        if (W > 1)
        {
            continue;
        }
        else
        {
            dst = r_ms * V_1 * sqrt(-2 * log(W) / W) + mean;
            ok = true;
        }
    }
    return dst;
}

//(05) 矩阵的基础算法函数
void vec_add(double vec_lef[], double vec_rig[], double vec_res[], int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        vec_res[i] = vec_lef[i] + vec_rig[i];
    }
}

void vec_minus(double vec_lef[], double vec_rig[], double vec_res[], int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        vec_res[i] = vec_lef[i] - vec_rig[i];
    }
}

double vec_inner_mul(double vec_1[], double vec_2[], int n)
{
    double res = 0;
    int i;
    for (i = 0 ; i < n ; i++)
    {
        res += vec_1[i] * vec_2[i];
    }
    return res;
}

void vec_outer_mul(double vec_1[3], double vec_2[3], double res[3])
{
    res[0] = vec_1[1] * vec_2[2] - vec_1[2] * vec_2[1];
    res[1] = vec_1[2] * vec_2[0] - vec_1[0] * vec_2[2];
    res[2] = vec_1[0] * vec_2[1] - vec_1[1] * vec_2[0];
}

double vec_length(double vec[3])
{
    double res = vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2];
    res = sqrt(res);
    return res;
}

double vec_angle(double vec_1[3], double vec_2[3])
{
    double cos_theta = vec_inner_mul(vec_1, vec_2, 3) / (vec_length(vec_1) * vec_length(vec_2));
    double theta = acos(cos_theta);
    return theta;
}

void mat_mul(double * left, int left_row, int left_col, double * right, int right_row, int right_col, double * res, int res_row, int res_col)
{
    if (left_col != right_row || res_row != left_row || res_col != right_col)
    {
        return;
    }
    else
    {
        int ind_lef = 0;
        int ind_rig = 0;
        int ind_res = 0;
        for (int i = 0; i < res_row; i++)
        {
            for (int j = 0; j < res_col; j++)
            {
                ind_res = i * res_col + j;
                res[ind_res] = 0;
                for (int k = 0; k < left_col; k++)
                {
                    ind_lef = i * left_col + k;
                    ind_rig = k * right_col + j;
                    res[ind_res] += left[ind_lef] * right[ind_rig];
                }
            }
        }
        return;
    }
}

void mat_mul(double ** left, int left_row, int left_col, double ** right, int right_row, int right_col, double ** res, int res_row, int res_col)
{
    if (left_col != right_row || res_row != left_row || res_col != right_col)
    {
        return;
    }
    else
    {
        for (int i = 0; i < res_row; i++)
        {
            for (int j = 0; j < res_col; j++)
            {
                res[i][j] = 0;
                for (int k = 0; k < left_col; k++)
                {
                    res[i][j] += left[i][k] * right[k][j];
                }
            }
        }
        return;
    }
}

void mat_tra(double ** src, int src_row, int src_col, double ** dst, int dst_row, int dst_col)
{
    if (src_row != dst_col || src_col != dst_row)
    {
        return;
    }
    else
    {
        for (int i = 0; i < src_row ; i++)
        {
            for (int j = 0 ; j < src_col ; j++)
            {
                dst[j][i] = src[i][j];
            }
        }
    }
}

void mat_to_vec(double ** src, int row, int col, double * dst, int cnt)
{
    if (cnt != row*col)
    {
        return;
    }
    else
    {
        for (int i = 0; i < row ; i++)
        {
            for (int j = 0 ; j < col ; j++)
            {
                dst[i * col + j] = src[i][j];
            }
        }
    }
}

void vec_to_mat(double * src, int cnt, double ** dst, int row, int col)
{
    if (cnt != row*col)
    {
        return;
    }
    else
    {
        for (int i = 0; i < row ; i++)
        {
            for (int j = 0 ; j < col ; j++)
            {
                dst[i][j] = src[i * col + j];
            }
        }
    }
}

int mat_inv(double *a, int n)
{
    int *is,  *js, i, j, k, l, u, v;
    double d, p;
    is = (int*)malloc(n * sizeof(int));
    js = (int*)malloc(n * sizeof(int));
    for (k = 0; k <= n - 1; k++)
    {
        d = 0.0;
        for (i = k; i <= n - 1; i++)
        {
            for (j = k; j <= n - 1; j++)
            {
                l = i * n + j;
                p = fabs(a[l]);
                if (p > d)
                {
                    d = p;
                    is[k] = i;
                    js[k] = j;
                }
            }
        }
        if (d + 1.0 == 1.0)
        {
            free(is);
            free(js);
            cout << "err**not inv\n" << endl;
            return (0);
        }
        if (is[k] != k)
        {
            for (j = 0; j <= n - 1; j++)
            {
                u = k * n + j;
                v = is[k] * n + j;
                p = a[u];
                a[u] = a[v];
                a[v] = p;
            }
        }
        if (js[k] != k)
        {
            for (i = 0; i <= n - 1; i++)
            {
                u = i * n + k;
                v = i * n + js[k];
                p = a[u];
                a[u] = a[v];
                a[v] = p;
            }
        }
        l = k * n + k;
        a[l] = 1.0 / a[l];
        for (j = 0; j <= n - 1; j++)
        {
            if (j != k)
            {
                u = k * n + j;
                a[u] = a[u] * a[l];
            }
        }
        for (i = 0; i <= n - 1; i++)
        {
            if (i != k)
            {
                for (j = 0; j <= n - 1; j++)
                {
                    if (j != k)
                    {
                        u = i * n + j;
                        a[u] = a[u] - a[i *n + k] * a[k *n + j];
                    }
                }
            }
        }
        for (i = 0; i <= n - 1; i++)
        {
            if (i != k)
            {
                u = i * n + k;
                a[u] =  - a[u] * a[l];
            }
        }
    }
    for (k = n - 1; k >= 0; k--)
    {
        if (js[k] != k)
        {
            for (j = 0; j <= n - 1; j++)
            {
                u = k * n + j;
                v = js[k] * n + j;
                p = a[u];
                a[u] = a[v];
                a[v] = p;
            }
        }
        if (is[k] != k)
        {
            for (i = 0; i <= n - 1; i++)
            {
                u = i * n + k;
                v = i * n + is[k];
                p = a[u];
                a[u] = a[v];
                a[v] = p;
            }
        }
    }
    free(is);
    free(js);
    return (1);
}

double mat_det(double *A, int m)
{
    int i = 0, ii = 0, j = 0, jj = 0, k = 0, t = 0, t1 = 1;
    double det = 1, mk = 0;

    double *pA = (double*)malloc(m * m * sizeof(double));
    double *pB = (double*)malloc(m * sizeof(double));
    for (i = 0; i < m; i++)
    {
        pB[i] = 0;
        for (j = 0; j < m; j++)
        {
            pA[i *m + j] = A[i *m + j];
        }
    }
    for (k = 0; k < m; k++)
    {
        for (j = k; j < m; j++)
        {
            if (pA[k *m + j])
            {
                for (i = 0; i < m; i++)
                {
                    pB[i] = pA[i *m + k];
                    pA[i *m + k] = pA[i *m + j];
                    pA[i *m + j] = pB[i];
                }
                if (j - k)
                {
                    t1 = t1 * (- 1);
                }
                t = t + 1;
                break;
            }
        }
        if (t)
        {
            for (ii = k + 1; ii < m; ii++)
            {
                mk = (- 1) * pA[ii *m + k] / pA[k *m + k];
                pA[ii *m + k] = 0;
                for (jj = k + 1; jj < m; jj++)
                {
                    pA[ii *m + jj] = pA[ii *m + jj] + mk * pA[k *m + jj];
                }
            }
            det = det * pA[k *m + k];
            t = 0;
        }
        else
        {
            det = 0;
            break;
        }
    }
    det = det * t1;
    free(pA);
    free(pB);
    pA = NULL;
    pB = NULL;
    return det;
}
*/



