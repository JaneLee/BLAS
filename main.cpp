#include <iostream>
#include <BLAS.h>

using namespace std;


int main()
{
    
    /*
    CMatrix<double, 2, 2> mat_a, mat_b;
    
    CMatrix<double, 2, 2> mat_add, mat_minus, mat_mul, mat_divide;

    // index search ok
    mat_b(0,0) = 1;
    mat_b(0,1) = 2;
    mat_b(1,0) = 3;
    mat_b(1,1) = 4;
    
    cout << "Mat a is: " << endl;
    printMat(mat_a);
    
    cout << "Mat b is: " << endl;
    printMat(mat_b);
    
    cout << "Mat add is: " << endl;
    printMat(mat_a + mat_b);
    
    cout << "Mat minus is: " << endl;
    printMat(mat_a - mat_b);
    
    cout << "Mat mul is: " << endl;
    printMat(mat_a * mat_b);
    
    
    */
    
    /*
    CMatrix<double, 3, 2> mat_a;
    CMatrix<double, 2, 3> mat_b;
    
    mat_a(0, 0) = 1; mat_a(0, 1) = 2;
    mat_a(1, 0) = 3; mat_a(1, 1) = 4;
    
    mat_b(0, 0) = 1; mat_b(0, 1) = 2;
    mat_b(1, 0) = 3; mat_b(1, 1) = 4;
    
    cout << "Mat a is: " << endl;
    cout <<  mat_a << endl;
    
    cout << "Mat b is: " << endl;
    cout <<  mat_b << endl;
    
    CMatrix<double, 3, 3> mat_c;
    mat_c = mat_a * mat_b;
    
    cout << "Mat a is: " << endl;
    cout <<  mat_a << endl;
    
    cout << "Mat b is: " << endl;
    cout <<  mat_b<< endl;
    
    cout << "Mat mul is: " << endl;
    cout <<  mat_c << endl;
    
    cout <<  mat_c.det() <<  endl;
    */
    
    /*
    CMatrix<double, 3, 2> mat_a;
    CVector<double, 2> vec_a;
    CRVector<double , 2> rvec_a;
    
    vec_a(0) = 1;
    vec_a(1) = 2;
    
    cout << "Transposed matrix is: " << endl;
    cout << mat_a.transpose() << endl;
    
    rvec_a = vec_a.transpose();
    cout <<  "Tranposed vector is: " << endl <<  rvec_a << endl;
    
    cout << "Mat and vector mul is: " << endl;
    cout <<  rvec_a * mat_a.transpose() << endl;
    
    cout <<  vec_a <<  endl;
    cout << "a norm is: " << vec_a.norm() << endl;
    cout <<  mat_a * vec_a << endl;
    
    CVector<double, 2> vec_normalized = vec_a.normalize();
    
    cout << "Normalized vector is: " <<  endl <<  vec_normalized <<  endl;
    */
   
   /*
    CVector<double, 3> vec_a, vec_b, vec_c;
    vec_a(0) = 0; vec_a(1) = 1; vec_a(2) = 2;
    vec_b(0) = 0; vec_b(1) = 1; vec_b(2) = 2;
    
    cout << "Mat a cross matrix is: " << endl << vec_a.cross_matrix() << endl;
    cout << "Mat a and mat b cross product is: " <<  endl <<   vec_a.cross_mul(vec_b) << endl;
    cout << "Mat a and mat b dot product is: " <<  endl << vec_a.dot_mul(vec_b) << endl;
   
   */
  
    CMatrix<double, 3, 3> mat_i;
    CVector<double, 3> vec_i;
    
    vec_i(0) = 1; vec_i(1) = 2; vec_i(2) = 3;
    
    mat_i(0, 0) = 2.5f;mat_i(0, 1) = 0;mat_i(0, 2) = 0;
    mat_i(1, 0) = 1;mat_i(1, 1) = 1.04f;mat_i(1, 2) = 1;
    mat_i(2, 2) = 0;mat_i(2, 1) = 1.01f;mat_i(2, 2) = 0;
    
    //mat_i(0, 0) = 1; mat_i(1, 1) = 1; mat_i(2, 2) = 1;
    cout << mat_i << endl;
    cout << mat_i * vec_i << endl;
    cout <<  mat_i.inv() * vec_i << endl;
    
    //cout << "Mat divide is: " << endl;
    //printMat(mat_b.inv() * mat_a);
    
    //cout << "Test BLAS: " << mat_a * mat_b << endl;
    return 0;
}

