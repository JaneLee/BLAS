TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
    BLAS.h

SOURCES += main.cpp \
    BLAS.cpp

include(deployment.pri)
qtcAddDeployment()

