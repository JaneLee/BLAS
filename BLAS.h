#ifndef BLAS_H
#define BLAS_H

#include <iostream>
#include <assert.h>
#include <cmath>

using namespace std;

//Gauss随机数j
//int MyRandomInt();
//double Gauss(double sys_err_ave,double sys_err_var);

//向量内积
//double vec_inner_mul(double vec_1[], double vec_2[], int n);

//向量外积
//void vec_outer_mul(double vec_1[3], double vec_2[3], double res[3]);

//向量求夹角
//double vec_angle(double vec_1[3], double vec_2[3]);

//矩阵转置
//void mat_tra(double ** src, int src_row, int src_col, double ** dst, int dst_row, int dst_col);

//矩阵变为一维向量
//void mat_to_vec(double ** src, int row, int col, double * dst, int cnt);

//向量变成矩阵
//void vec_to_mat(double * src, int cnt, double ** dst, int row, int col);

/*
 * template class should be header only, so all the function are implemented in this file
 */

// forward declare of template class Matrix
template<class T, int row_size, int col_size>
class CMatrix;

// forward declaration of template class col Vector
template<class T, int col_size>
class CVector;

// forward declaration of template class row vector
template<class T, int col_size>
class CRVector;

// template class CMatrix
template<class T, int row_size, int col_size>
class CMatrix
{
public:
    CMatrix();
    ~CMatrix();

    // getters
    inline int rows(){return rows_;}
    inline int cols(){return cols_;}
    inline int size() {return number_size_;}

    // Methods
    // override +
    CMatrix<T, row_size, col_size> operator+(const CMatrix<T, row_size, col_size> b);

    // override -
    CMatrix<T, row_size, col_size> operator-(const CMatrix<T, row_size, col_size> b);

    // override *
    template<class type, int l_rows, int l_cols, int r_rows, int r_cols>
    friend CMatrix<type, l_rows, r_cols> operator*(CMatrix<type, l_rows, l_cols> a, CMatrix<type, r_rows, r_cols> b);

    // override =
    void operator=(CMatrix<T, row_size, col_size> b);

    // overide ==
    bool operator==(const CMatrix<T, row_size, col_size>& b) const;

    // override output <<
    template<class type, int m_rows, int m_cols>
    friend ostream& operator<< (ostream& stream, CMatrix<type, m_rows, m_cols> cmat);
    
    //  get the cofator of element i and j of matrix 
    CMatrix<T, row_size - 1,  col_size -1> cofactor(int i, int j);
    
    // 
    CMatrix<T, col_size, row_size> transpose();
    
    // get one row of the matrix
    CRVector<T, col_size> row(int row_idx);
    
    // get one column of the matrix
    CVector<T, row_size> col(int col_idx);
    
    // override ()
    // input:
    //       row_idx: row index
    //       col_idx: col index
    // output:
    //       T&: reference to data matrix element
    T& operator()(int row_idx, int col_idx) const;

    // get the row of matrix

    // return the inverse of matrix
    CMatrix<T, row_size, col_size> inv();
    
    // return the determinant of matrix
    T det();

    void swaprow(int row_idx1, int row_idx2);
    void swapcol(int col_idx1, int col_idx2);
    
private:
    // store the data in column major
    int rows_;
    int cols_;
    int number_size_;
    T* data_;
};


// CMatrix constructor
template<class T, int row_size, int col_size>
CMatrix<T, row_size, col_size>::CMatrix()
{
    int number_size_ = row_size * col_size;
    data_ = new T[row_size * col_size];

    rows_ = row_size;
    cols_ = col_size;
    
    //assert(rows_ == cols_);

    for(int i = 0; i < number_size_; i++)
        data_[i] = 0;
}

// CMatrix deconstructor
template<class T, int row_size, int col_size>
CMatrix<T, row_size, col_size>::~CMatrix()
{
    //delete data_;
}

// Matrix + operator
template<class T, int row_size, int col_size>
CMatrix<T, row_size, col_size> CMatrix<T, row_size, col_size>::operator+(const CMatrix<T, row_size, col_size> b)
{
    CMatrix<T, row_size, col_size> r_mat;

    for( int i = 0; i < rows_; i++)
    {
        for(int j = 0; j < cols_; j++)
        {
            r_mat(i,j) = this->operator()(i, j) + b(i, j);
        //cout << b(i, j) <<  " " << endl;
        //cout <<  r_mat(i, j) << " ";
        }
    }
    return r_mat;
}

// Matrix - operator
template<class T, int row_size, int col_size>
CMatrix<T, row_size, col_size> CMatrix<T, row_size, col_size>::operator-(const CMatrix<T, row_size, col_size> b)
{
    CMatrix<T, row_size, col_size> r_mat;

    for( int i = 0; i < rows_; i++)
    {
        for(int j = 0; j < cols_; j++)
        {
            r_mat(i,j) = this->operator()(i, j) - b(i, j);
            //cout << b(i, j) <<  " " << endl;
            //cout <<  r_mat(i, j) << " ";
        }
    }
    return r_mat;
}

// Matrix * operator
template<class T, int l_rows, int l_cols, int r_rows, int r_cols>
CMatrix<T, l_rows, r_cols> operator*(CMatrix<T, l_rows, l_cols> a, CMatrix<T, r_rows, r_cols> b)
{
    CMatrix<T, l_rows, r_cols> r_mat;

    // the matrix a col size must equal to b row size
    assert(a.cols() == b.rows());

    for( int i = 0; i < a.rows(); i++)
        for(int j = 0; j < b.cols(); j++)
        {
            T res = 0;
            for(int k = 0; k < a.cols(); k++)
            {                
                res = res + a(i, k) * b(k, j);
            }

            r_mat(i, j) = res;
        }

    return r_mat;
}


// get one row of the matrix
template<class T, int row_size, int col_size>
CRVector<T, col_size> CMatrix<T, row_size, col_size>::row(int row_idx)
{
    assert(row_idx >= 0 && row_idx < row_size);
    
    CRVector<T, col_size> res_rvec;
    
    for (int i = 0; i < col_size; i++)
    {
        res_rvec(i) = this->operator()(row_idx, i);
    }
    
    return res_rvec;
}

// get one column of the matrix
template<class T, int row_size, int col_size>
CVector<T, row_size> CMatrix<T, row_size, col_size>::col(int col_idx)
{
    assert(col_idx >= 0 && col_idx < col_size);
    
    CVector<T, row_size> res_vec;
    
    for (int i = 0; i < row_size; i++)
    {
        res_vec(i) = this->operator()(i, col_idx);
    }
    
    return res_vec;
}

// change two row
template<class T, int row_size, int col_size>
void CMatrix<T, row_size, col_size>::swaprow(int row_idx1, int row_idx2)
{
    assert(row_idx1 >= 0 && row_idx1 < row_size &&
    row_idx2 >= 0 && row_idx2 < row_size);
    
    T t;
    for (int i = 0; i < col_size; i++)
    {
        t = this->operator()(row_idx1, i);
        this->operator()(row_idx1, i) = this->operator()(row_idx2, i);
        this->operator()(row_idx2, i) = t;
    }
}

// change two column
template<class T, int row_size, int col_size>
void CMatrix<T, row_size, col_size>::swapcol(int col_idx1, int col_idx2)
{
    assert(col_idx1 >= 0 && col_idx1 < col_size &&
        col_idx2 >= 0 && col_idx2 < col_size);

    T t;
    for (int i = 0; i < row_size; i++)
    {
        t = this->operator()(i, col_idx1);
        this->operator()(i, col_idx1) = this->operator()(i, col_idx2);
        this->operator()(i, col_idx2) = t;
    }
}

// get the cofator of matrix element (i, j)
template<class T, int row_size, int col_size>
CMatrix<T, row_size - 1, col_size - 1> CMatrix<T, row_size, col_size>::cofactor(int row_idx, int col_idx)
{
    CMatrix<T, row_size - 1,  col_size -1> cofactor_mat;
    
    for (int i = 0; i < row_size; i++)
    {
        if (i == row_idx)
            continue;
        for (int j = 0; j < col_size; j++)
        {
            if (j == col_idx)
                continue;
            else
            {
                if (i < row_idx )
                {
                    if (j < col_idx)
                        cofactor_mat(i, j) = this->operator()(i, j);
                    else
                        cofactor_mat(i, j - 1) = this->operator()(i, j);
                }
                else
                {
                    if (j < col_idx)
                        cofactor_mat(i - 1, j) = this->operator()(i, j);
                    else
                        cofactor_mat(i - 1, j - 1) = this->operator()(i, j);
                }
            }
        }
    }
}

// Matrix operator = override
template<class T, int row_size, int col_size>
void CMatrix<T, row_size, col_size>::operator=(CMatrix<T, row_size, col_size> b)
{
    assert(b.cols() ==  this->cols() && b.rows() ==  this->rows());
    
    // column major
    for(int i = 0; i < rows_; i++)
        for(int j = 0; j < cols_; j++)
        {
            this->operator ()(i, j) = b(i, j);
        }
}

template<class T, int row_size, int col_size>
CMatrix<T, col_size, row_size> CMatrix<T, row_size, col_size>::transpose()
{
    CMatrix<T, col_size, row_size> mat_trans;
    
    for (int i = 0; i < col_size; i++)
        for (int j = 0; j < row_size; j++)
            mat_trans(i, j) = this->operator()(j, i);
            
    return mat_trans;
}

// Matrix inv method
template<class T, int row_size, int col_size>
CMatrix<T, row_size, col_size> CMatrix<T, row_size, col_size>::inv()
{
    // only square matrix have inverse matrix
    assert(row_size == col_size && row_size > 0);
    
    // 
    CMatrix<T, row_size, col_size> inv_mat;
    for (int i = 0; i < row_size; i++)
        inv_mat(i, i) = 1;
    
    // 
    if (row_size ==  1)
    {
        inv_mat(0, 0) = 1.0/this->operator()(0, 0);
        return inv_mat;
    }
    else
    {
        // get the inverse matrix by gauss jordan method
        // http://www.sanfoundry.com/cpp-program-implement-gauss-jordan-elimination/
        // partial pivoting 
        for (int i = 0; i  < row_size; i++)
        {
            // the m(i, i) must be non-zero
            bool found_non_zeros_elem = false;
            for (int j = i; j < row_size; j++)
            {
                if (this->operator()(j, i) !=  0)
                {
                    // swap row i and row j
                    if (i != j)
                    {
                        this->swaprow(i, j);
                        inv_mat.swaprow(i, j);
                    }
                    found_non_zeros_elem = true;
                        break;                    
                }       
            }
            
            /*
            cout << "After swap" << endl;
            cout << "Raw mat" << endl;
            for (int m = 0; m < row_size; m++)
            {
                for (int n = 0; n < col_size; n++)
                    cout << this->operator()(m, n) <<  " ";
                cout << endl;
            }
            
            cout << "Inv mat" << endl;
            for (int m = 0; m < row_size; m++)
            {
                for (int n = 0; n < col_size; n++)
                    cout << inv_mat(m, n) << " ";
                cout << endl;
            }
            */
                      
            // this matrix must be invertable
            assert(found_non_zeros_elem !=  false);
            
            // diagonal element to unit element
            T div = this->operator()(i, i);
            for (int k = i; k < col_size; k++)
            {
                this->operator()(i, k) = this->operator()(i, k) / div;
            }
            for (int k = 0; k < col_size; k++)
            {
                inv_mat(i, k) = inv_mat(i, k) / div;
            }
            // elimate the below row 
            for (int j = i + 1; j < row_size; j++)
            {
                div = this->operator()(j, i);
                for (int k = i; k < col_size; k++)
                {
                    this->operator()(j, k) += -(T)div * this->operator()(i, k); 
                }
                
                for (int k = 0; k < col_size; k++)
                {
                    inv_mat(j, k) += -div * inv_mat(i, k);
                }
            }
            
            /*
            cout << "After elimation" << endl;
            cout << "Raw mat" << endl;
            for (int m = 0; m < row_size; m++)
            {
                for (int n = 0; n < col_size; n++)
                cout << this->operator()(m, n) <<  " ";
                cout << endl;
            }
            
            cout << "Inv mat" << endl;
            for (int m = 0; m < row_size; m++)
            {
                for (int n = 0; n < col_size; n++)
                cout << inv_mat(m, n) << " ";
                cout << endl;
            }
            */
        }
        
        // this matrix is not invertable
        assert(this->operator()(col_size - 1, row_size - 1) != 0);
            
        // convert to diagonal matrix
        for (int i = row_size - 1; i >=  0; i--)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                T mul = this->operator()(j, i);
                if (mul !=  0)
                {
                    this->operator()(j, i) = 0;
                    // change inv mat
                    for (int k = 0; k < col_size; k++)
                    {
                        inv_mat(j, k) -= mul * inv_mat(i, k);
                    }
                }                
            }
        }
        
        /*
        cout << "After elimation upper" << endl;
        cout << "Raw mat" << endl;
        for (int m = 0; m < row_size; m++)
        {
            for (int n = 0; n < col_size; n++)
            cout << this->operator()(m, n) <<  " ";
            cout << endl;
        }

        cout << "Inv mat" << endl;
        for (int m = 0; m < row_size; m++)
        {
            for (int n = 0; n < col_size; n++)
            cout << inv_mat(m, n) << " ";
            cout << endl;
        }
        */
        return inv_mat;
    }
    
}

// Matrix determinant method
template<class T, int row_size, int col_size>
T CMatrix<T, row_size, col_size>::det()
{
    // only square matrix have determinant function
    assert(row_size == col_size && row_size > 0);
    
    if (row_size == 1)
        return this->operator()(0, 0);
    else if (row_size == 2)
    {
        return this->operator()(0, 0) * this->operator()(1, 1) - 
            this->operator()(1, 0) * this->operator()(0, 1);
    }
    else 
    {
        // calculate the determinant by gaussian elimation
        // guarentee all the column are non-zero
        // http://stackoverflow.com/questions/25648966/determinant-of-a-matrix-by-gaussian-elimination-c
        for (int j = 0; j < col_size; j++)
        {
            bool found_non_zeros_elem = false;
            for (int i = j; i < row_size; i++)
            {
                if (this->operator()(i, j) !=  0)
                {
                    // swap row i and row j
                    if (i != j)
                        this->swaprow(i, j);
                    
                    found_non_zeros_elem = true;
                    break;                    
                }                
            }
            
            // 
            if (!found_non_zeros_elem)
                return 0;
                
            // 
            for (int i = j + 1; i < row_size; i++)
            {
                while (true)
                {
                    // get the divide
                    T del = this->operator()(i, j) /this->operator()(j, j);
                    for (int k = j; k < col_size; k++)
                    {
                        this->operator()(i, k) -= del * this->operator()(j, k);                        
                    }
                    
                    if (this->operator()(i, j) ==  0)
                    {
                        break;
                    }
                    else
                    {
                        swaprow(i, j);
                    }
                }
            }
        }        
        
        /*
        for (int i = 0; i < row_size; i++)
        {
            for (int j = 0; j < col_size; j++)
            {
                cout <<  this->operator()(i, j) << " ";
            }
            cout << endl;
        }
        */
        
        
        T res = 1;
        for ( int i = 0; i < row_size; i++)
            res = res * this->operator()(i, i);
        return res;
    }
}

template<class T, int row_size, int col_size>
T& CMatrix<T, row_size, col_size>::operator()(int row_idx, int col_idx) const 
{
    assert(row_idx < row_size && col_idx < col_size);
    return data_[row_idx *  col_size + col_idx];
}

template<class T, int row_size, int col_size>
ostream& operator<<(ostream& stream, CMatrix<T, row_size, col_size> cmat)
{
    for (int i = 0; i < row_size; i++)
    {
        for (int j = 0; j < col_size; j++)
        {
            cout <<  cmat(i, j) << " ";
        }
        cout <<  endl;
    }
}

// derive Vector class from Matrix class
template<class T, int row_size>
class CVector: public CMatrix<T, row_size, 1>
{
public:
    CVector();
    ~CVector();

    // getter of data
    T& operator()(int row_idx);
    
    // get the norm of function
    T norm();
    
    // normalize the vector
    CVector<T, row_size> normalize();
    
    // vector dot mul
    T dot_mul(CVector<T, row_size>);
    
    // to cross matrix
    CMatrix<T, row_size, row_size> cross_matrix();
    
    // cross mul 
    CVector<T, row_size> cross_mul(CVector<T, row_size> a);
    
    // 
    CRVector<T, row_size> transpose();
    
    void operator=(CMatrix<T, row_size, 1> b);
    
    void operator=(CVector<T, row_size> b);
    
    
private:
};

template<class T,  int row_size>
CVector<T, row_size>::CVector()
{
    
}

template<class T, int row_size>
CVector<T, row_size>::~CVector()
{

}

// override operator()
template<class T, int row_size>
T& CVector<T, row_size>::operator()(int row_idx)
{
    return CMatrix<T, row_size, 1>::operator()(row_idx, 0);
}

// get the norm of 

template<class T, int row_size>
T CVector<T, row_size>::norm()
{
    T res = 0;
    for (int i = 0; i < row_size; i++)
    {
        res +=  pow(this->operator()(i), 2);
    }
    return sqrt(res);
}

// normalize vector
template<class T,  int row_size>
CVector<T, row_size> CVector<T, row_size>::normalize()
{
    CVector<T, row_size> res_vec;
    
    T norm = this->norm();
    
    for (int i = 0; i < row_size; i++)
    {
        res_vec(i) = this->operator()(i) / norm;            
    }
    
    return res_vec;
}

template<class T, int row_size>
void CVector<T, row_size>::operator=(CMatrix<T, row_size, 1> b)
{
    for (int i = 0; i < row_size; i++)
    {
        this->operator()(i) = b(i, 0);
    }
}

template<class T, int row_size>
void CVector<T, row_size>::operator=(CVector<T, row_size> b)
{
    for (int i = 0; i < row_size; i++)
    {
        this->operator()(i) = b(i);
    }
}

// tranpose a vector
template<class T,  int row_size>
CRVector<T, row_size> CVector<T, row_size>::transpose()
{
    CRVector<T, row_size> rvec_res;
    for (int i = 0; i < row_size; i++)
    {
        rvec_res(i) = this->operator()(i);
    }
    
    return rvec_res;
}

// dot mul  
template<class T, int row_size>
T CVector<T, row_size>::dot_mul(CVector<T, row_size> a)
{
    T res = 0;
    for (int i = 0; i < row_size; i++)
    {
        res += this->operator()(i) * a(i);
    }
    
    return res;
}

// cross matrix
template<class T,  int row_size>
CMatrix<T, row_size, row_size> CVector<T, row_size>::cross_matrix()
{
    // this function is designed specifical for 3 dimension vector
    if (row_size !=  3)
        cout << "==To use the cross matrix, the vector must be 3 dimension!" << endl;
        
    assert(row_size == 3);
    CMatrix<T, row_size, row_size> res_mat;
    res_mat(0, 1) = - this->operator()(2);
    res_mat(0, 2) = this->operator()(1);
    res_mat(1, 0) = this->operator()(2);
    res_mat(1, 2) = - this->operator()(0);
    res_mat(2, 0) = -this->operator()(1);
    res_mat(2, 1) = this->operator()(0);
    
    return res_mat;
}

// cross product
template<class T, int row_size>
CVector<T, row_size> CVector<T, row_size>::cross_mul(CVector<T, row_size> a)
{
    if (row_size !=  3)
        cout << "==To use cross multiply, the vector must be 3 dimension"
    assert(row_size == 3);
    
    CVector<T, row_size> res_vec;
    res_vec = this->cross_matrix() * a;
    return res_vec;
}


// derive Vector class from Matrix class
template<class T, int col_size>
class CRVector: public CMatrix<T, 1, col_size>
{
public:
    CRVector();
    ~CRVector();

    // getter of data
    T& operator()(int col_idx);

private:
    
};

template<class T,  int col_size>
CRVector<T, col_size>::CRVector()
{

}

template<class T, int col_size>
CRVector<T, col_size>::~CRVector()
{

}

template<class T, int col_size>
T& CRVector<T, col_size>::operator()(int col_idx)
{
    return CMatrix<T, 1, col_size>::operator()(0, col_idx);
}

#endif // BLAS_H
